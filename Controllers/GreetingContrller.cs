﻿using Microsoft.AspNetCore.Mvc;

namespace Greeting.Controllers
{
    public class GreetingContrller : Controller
    {
        [HttpGet("/")]
        public string Get(string name = "Mundo")
        {
            return "Hola, " + name + "!!";
        }
        
    }
}
